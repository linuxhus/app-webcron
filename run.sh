#!/bin/sh

case $1 in 
	start)
		nohup ./webcronen 2>&1 >> info.log 2>&1 /dev/null &
		echo "Service has started......"
		sleep 1
	;;
	stop)
		killall webcronen
		echo "Service has stopped......"
		sleep 1
	;;
	restart)
		killall webcronen
		sleep 1
		nohup ./webcronen 2>&1 >> info.log 2>&1 /dev/null &
		echo "The service has been restarted......"
		sleep 1
	;;
	*) 
		echo "$0 {start|stop|restart}"
		exit 4
	;;
esac

