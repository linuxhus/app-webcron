CREATE TABLE `t_task` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user id',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT 'group id',
  `task_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'task name',
  `task_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'task type',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT 'description',
  `cron_spec` varchar(100) NOT NULL DEFAULT '' COMMENT 'cron spec',
  `concurrent` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'only one instance is allowed',
  `command` text NOT NULL COMMENT 'command',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'status 0 or 1',
  `notify` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'notify',
  `notify_email` text NOT NULL COMMENT 'notify email',
  `timeout` smallint(6) NOT NULL DEFAULT '0' COMMENT 'timeout',
  `execute_times` int(11) NOT NULL DEFAULT '0' COMMENT 'execution time',
  `prev_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last execution time',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Creation time',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_task_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user id',
  `group_name` varchar(50) NOT NULL DEFAULT '' COMMENT 'group name',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT 'description',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT 'creation time',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `t_task_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'task id',
  `output` mediumtext NOT NULL COMMENT 'output',
  `error` text NOT NULL COMMENT 'error',
  `status` tinyint(4) NOT NULL COMMENT 'status',
  `process_time` int(11) NOT NULL DEFAULT '0' COMMENT 'Consumption time / millisecond',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'creation time',
  PRIMARY KEY (`id`),
  KEY `idx_task_id` (`task_id`,`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `t_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT 'user name',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT 'email',
  `password` char(32) NOT NULL DEFAULT '' COMMENT 'password',
  `salt` char(10) NOT NULL DEFAULT '' COMMENT 'slat',
  `last_login` int(11) NOT NULL DEFAULT '0' COMMENT 'last login',
  `last_ip` char(15) NOT NULL DEFAULT '' COMMENT 'last ip',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'status',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_user` (`id`, `user_name`, `email`, `password`, `salt`, `last_login`, `last_ip`, `status`)
VALUES (1,'admin','admin@example.com','7fef6171469e80d32c0559f88b377245','',0,'',0);